# Image Analysis Microservice #

Microservice to get image information using the Google Cloud Vision API.

http://vision-microservice.herokuapp.com

### What is this repository for? ###

* Pass an image in to a URL via query string to receive a json repsonse containing image info
* Version 1.0

### How do I get set up? ###

* Clone repo
* Install deps
* npm start

### Params ###

* image [url to remote or local image]
* detect [ crops | document | faces | landmarks | labels | logos | properties | safeSearch | similar | text ]

### Example usage ###


```
#!js

axios.get('http://vision-microservice.herokuapp.com', {
    params: {
      image: "http://myimage.com/image.png",
      detect: "en"
    }
  })
  .then(function (response) {
    console.log(response.data);
  })
  .catch(function (error) {
    console.log(error);
  });  
```

### Result ###


```
#!json

[
    [
        "fruit",
        "natural foods",
        "apple",
        "produce",
        "food"
    ],
    {
        "responses": [
            {
                "faceAnnotations": [],
                "landmarkAnnotations": [],
                "logoAnnotations": [],
                "labelAnnotations": [
                    {
                        "mid": "/m/02xwb",
                        "locale": "",
                        "description": "fruit",
                        "score": 0.9664963483810425,
                        "confidence": 0,
                        "topicality": 0,
                        "boundingPoly": null,
                        "locations": [],
                        "properties": []
                    },
                    {
                        "mid": "/m/08tlbj",
                        "locale": "",
                        "description": "natural foods",
                        "score": 0.9654038548469543,
                        "confidence": 0,
                        "topicality": 0,
                        "boundingPoly": null,
                        "locations": [],
                        "properties": []
                    },
                    {
                        "mid": "/m/014j1m",
                        "locale": "",
                        "description": "apple",
                        "score": 0.9599219560623169,
                        "confidence": 0,
                        "topicality": 0,
                        "boundingPoly": null,
                        "locations": [],
                        "properties": []
                    },
                    {
                        "mid": "/m/036qh8",
                        "locale": "",
                        "description": "produce",
                        "score": 0.9327006936073303,
                        "confidence": 0,
                        "topicality": 0,
                        "boundingPoly": null,
                        "locations": [],
                        "properties": []
                    },
                    {
                        "mid": "/m/02wbm",
                        "locale": "",
                        "description": "food",
                        "score": 0.7920650243759155,
                        "confidence": 0,
                        "topicality": 0,
                        "boundingPoly": null,
                        "locations": [],
                        "properties": []
                    }
                ],
                "textAnnotations": [],
                "fullTextAnnotation": null,
                "safeSearchAnnotation": null,
                "imagePropertiesAnnotation": null,
                "cropHintsAnnotation": null,
                "webDetection": null,
                "error": null
            }
        ]
    }
]
```