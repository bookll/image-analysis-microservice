require('dotenv').config()

const {
    send
} = require('micro')
const url = require('url')

const Vision = require('@google-cloud/vision');

var vision = Vision({
    projectId: 'conductive-fold-172614',
    keyFilename: '../wammajam-microservices-229df2dd74ac.json'
});

module.exports = function(req, res) {

    const path = url.parse(req.url, true);
    const image = path.query.image || "image.png"; // https://www.goldennumber.net/wp-content/uploads/2013/08/florence-colgate-england-most-beautiful-face.jpg
    const detect = path.query.detect || "labels"; // crops, document, faces, landmarks, labels, logos, properties, safeSearch, similar, text

    vision.detect(image, detect).then(data => {

        const apiResponse = JSON.stringify(data[1], null, 4);
        const detections = JSON.stringify(data[0], null, 4);

        res.write(JSON.stringify(data, null, 4));

        send(res, 200)

        console.log(apiResponse);

    }).catch(err => {
        console.error(err);
    });

}